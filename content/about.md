---
title: "About"
date: 2019-01-19T02:42:30+01:00
draft: false
---

{{< image src="/d.png" alt="Ravindra Sisodia" position="center" style="height:250px;float:left;">}}
{{< image src="/da.png" alt="Ravindra Sisodia" position="right" style="height:250px;border-radius:50%;">}}

Hello, I am Ravindra Sisodia.
<br />

## Online Trainings/Tests

1. The Fundamentals of Digital Marketing
3. Advanced PHP
2. Advanced Laravel
3. Building Serverless Apps on AWS


## Jobs

Since 2015: **Self employed Software Engineer and Cloud Consultant**

### Work History

- *02.2018 - 06.2019*: **Software Developer**, [Alina Softwares LLP](https://alinasoftwares.in/)
