---
title: "Works"
date: 2019-12-21T02:48:00+05:30
draft: false
---

## Tools

- [Smart Fuzzer](https://github.com/InfoSecRavindra/SmartFuzzer), Secrets Scanner in [Python](https://www.python.org/)
- [Twitter Cleaner](https://github.com/InfoSecRavindra/Twitter-Cleaner), Twitter Cleaner in [Python](https://www.python.org/)

## Projects

#### Girnar Plywood Inventory Management System

An inventory management system for plywood shop, features include stock management, purchase and sell management, payment management, reporting, email and SMS based reminder for payment and purchase.

**Customer:** [Girnar Plywood.][GIRNAR_PLYWOOD]

**Technologies:** [PHP][HOME_PHP], [Laravel][HOME_LARAVEL], [Vue][HOME_VUE]
<hr />

<hr />

[HOME_APO]:         https://apothekia.de
[HOME_ARANGO]:      https://www.arangodb.com
[HOME_AWS]:         https://aws.amazon.com
[HOME_BOOTSTRAP]:   https://getbootstrap.com
[HOME_BOXINE]:      https://tonies.de
[HOME_BREUNINGER]:  https://www.breuninger.com
[HOME_DJANGO]:      https://www.djangoproject.com
[HOME_DOCKER]:      https://www.docker.com
[HOME_ELASTIC]:     https://elastic.co
[HOME_ELIXIR]:      https://elixir-lang.org
[HOME_FLASK]:       http://flask.pocoo.org
[HOME_HSOWL]:       https://www.hs-owl.de
[HOME_INIT]:        https://www.hs-owl.de/init/
[HOME_JAVA]:        https://java.com
[HOME_KOTLIN]:      http://kotlinlang.org
[HOME_MSAZURE]:     https://azure.microsoft.com
[HOME_NODEJS]:      https://nodejs.org
[HOME_PHOENIX]:     https://phoenixframework.org
[HOME_PHP]:         https://php.net
[HOME_PROTOB]:      https://developers.google.com/protocol-buffers
[HOME_PSQL]:        https://www.postgresql.org
[HOME_PYTHON]:      https://python.org
[HOME_REACTJS]:     https://reactjs.org
[HOME_RABBITMQ]:    https://www.rabbitmq.com
[HOME_REDIS]:       https://redis.io
[HOME_SPRING]:      http://spring.io
[HOME_LARAVEL]:     https://laravel.com
[HOME_STST]:        https://stepstone.de
[HOME_SYMFONY]:     https://symfony.com
[HOME_TF]:          https://terraform.io
[HOME_TYPO3]:       https://typo3.org
[HOME_TYPOS]:       https://docs.typo3.org
[HOME_VUE]:         https://vuejs.org/
[HOME_WP]:          https://wordpress.com
[WIKI_C]:           https://en.wikipedia.org/wiki/C_(programming_language)
[WIKI_C++]:         https://en.wikipedia.org/wiki/C%2B%2B
[WIKI_C#]:          https://en.wikipedia.org/wiki/C_Sharp_(programming_language)
[WIKI_HTTP]:        https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol
[WIKI_IEC611313]:   https://en.wikipedia.org/wiki/IEC_61131-3
[WIKI_JS]:          https://en.wikipedia.org/wiki/JavaScript
[WIKI_JSON]:        https://en.wikipedia.org/wiki/JSON
[WIKI_KRL]:         https://en.wikipedia.org/wiki/KUKA_Robot_Language
[WIKI_SOAP]:        https://en.wikipedia.org/wiki/SOAP
[WIKI_XML]:         https://en.wikipedia.org/wiki/XML
[GIRNAR_PLYWOOD]:   https://girnarplywood.com